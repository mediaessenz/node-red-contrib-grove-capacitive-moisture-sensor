#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# The MIT License (MIT)
#
# Grove Base Hat for the Raspberry Pi, used to connect grove sensors.
# Copyright (C) 2018  Seeed Technology Co.,Ltd.
'''
This is the code for
    - `Grove -  Capacitive Moisture Sensor <https://wiki.seeedstudio.com/Grove-Capacitive_Moisture_Sensor-Corrosion-Resistant/)>`_
'''
import time, sys, io
from grove.adc import ADC

__all__ = ["GroveCapacitiveMoistureSensor"]

class GroveCapacitiveMoistureSensor(object):
    '''
    Grove Capacitive Moisture Sensor class

    Args:
        pin(int): number of analog pin/channel the sensor connected.
    '''
    def __init__(self, channel):
        self.channel = channel
        self.adc = ADC()

    @property
    def value(self):
        '''
        Get the moisture value.

        Returns:
            (int): ratio, 0(0.0%) - 1000(100.0%)
        '''
        return self.adc.read(self.channel)

Grove = GroveCapacitiveMoistureSensor


def main():
    from grove.helper import SlotHelper
    save_stdout = sys.stdout
    sys.stdout = io.BytesIO()
    sh = SlotHelper(SlotHelper.ADC)
    sys.stdout = save_stdout
    pin = sh.argv2pin()

    sensor = GroveCapacitiveMoistureSensor(pin)

    print(sensor.value)

if __name__ == '__main__':
    main()
